/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.ThrownExpBottle
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.entity.ExpBottleEvent
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.projectiles.ProjectileSource
 */
package kiki.cro.beastxp.listener;

import java.util.HashMap;
import java.util.List;
import kiki.cro.beastxp.Main;
import kiki.cro.beastxp.XpItem;
import kiki.cro.beastxp.XpLevel;
import kiki.cro.beastxp.YmlMaker;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.projectiles.ProjectileSource;

public class XpBottleThrow
implements Listener {
    private Main pl;

    public XpBottleThrow(Main plugin) {
        this.pl = plugin;
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && e.getPlayer().getItemInHand().getType().equals((Object)Material.EXP_BOTTLE)) {
            Block block = e.getClickedBlock();
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK && (block.getType() == Material.CHEST || block.getType() == Material.FURNACE || block.getType() == Material.BURNING_FURNACE || block.getType() == Material.ENDER_CHEST || block.getType() == Material.TRAPPED_CHEST || block.getType() == Material.ANVIL || block.getType() == Material.BED_BLOCK || block.getType() == Material.ENCHANTMENT_TABLE || block.getType() == Material.DISPENSER || block.getType() == Material.NOTE_BLOCK || block.getType() == Material.LEVER || block.getType() == Material.DIODE_BLOCK_OFF || block.getType() == Material.DIODE_BLOCK_ON || block.getType() == Material.SPRUCE_DOOR || block.getType() == Material.BIRCH_DOOR || block.getType() == Material.JUNGLE_DOOR || block.getType() == Material.ACACIA_FENCE_GATE || block.getType() == Material.DARK_OAK_FENCE_GATE || block.getType() == Material.JUNGLE_FENCE_GATE || block.getType() == Material.BIRCH_FENCE_GATE || block.getType() == Material.SPRUCE_FENCE_GATE || block.getType() == Material.WOOD_DOOR || block.getType() == Material.IRON_DOOR_BLOCK || block.getType() == Material.DARK_OAK_DOOR || block.getType() == Material.ACACIA_DOOR || block.getType() == Material.WORKBENCH || block.getType() == Material.HOPPER || block.getType() == Material.DROPPER || block.getType() == Material.FENCE_GATE || block.getType() == Material.TRAP_DOOR || block.getType() == Material.IRON_TRAPDOOR || block.getType() == Material.STONE_BUTTON || block.getType() == Material.WOOD_BUTTON || block.getType() == Material.BEACON || block.getType() == Material.CAULDRON)) {
                if (this.pl.xpitemlore.containsKey(e.getPlayer().getName())) {
                    this.pl.xpitemlore.remove(e.getPlayer().getName());
                }
                e.getPlayer().updateInventory();
                return;
            }
            Player p = e.getPlayer();
            String ExpName = this.pl.getCfg().getConfig().getString("BottleName");
            ExpName = ExpName.replaceAll("&", "\u00a7");
            if (!p.getItemInHand().hasItemMeta()) {
                if (this.pl.xpitemlore.containsKey(p.getName())) {
                    e.setCancelled(true);
                    p.updateInventory();
                }
                return;
            }
            if (!p.getItemInHand().getItemMeta().hasLore()) {
                return;
            }
            if (p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ExpName)) {
                if (this.pl.xpitemlore.containsKey(p.getName())) {
                    e.setCancelled(true);
                    p.updateInventory();
                    return;
                }
                String lore = (String)e.getPlayer().getItemInHand().getItemMeta().getLore().get(0);
                if (e.hasItem()) {
                    String[] values;
                    lore = ChatColor.stripColor((String)lore);
                    String[] arrstring = values = lore.split(" ");
                    int n = arrstring.length;
                    int n2 = 0;
                    while (n2 < n) {
                        String bottlevalue = arrstring[n2];
                        try {
                            int exp = Integer.parseInt(bottlevalue);
                            this.pl.xpitemlore.put(p.getName(), exp);
                        }
                        catch (NumberFormatException var12_12) {
                            // empty catch block
                        }
                        ++n2;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onExp(ExpBottleEvent e) {
        ThrownExpBottle pro = e.getEntity();
        if (!(pro.getShooter() instanceof Player)) {
            return;
        }
        Player p = (Player)pro.getShooter();
        if (!(pro instanceof ThrownExpBottle)) {
            return;
        }
        if (this.pl.xpitemlore.isEmpty()) {
            return;
        }
        int xp = this.pl.xpitemlore.get(p.getName());
        e.setExperience(xp);
        this.pl.xpitemlore.remove(p.getName());
        p.updateInventory();
    }

    @EventHandler
    public void PlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if (!p.hasPermission("exp.drop")) {
            return;
        }
        int xp = XpLevel.getXp(p);
        if (xp <= 0) {
            return;
        }
        double dropPercentage = this.pl.getCfg().getConfig().getDouble("DropPercentage") / 100.0;
        xp = (int)((double)xp * dropPercentage);
        this.pl.xpBottle.setXpb(xp, p);
        ItemStack Xpb = this.pl.xpBottle.getXpb();
        p.getWorld().dropItem(p.getLocation(), Xpb);
        p.setTotalExperience(0);
        p.setLevel(0);
        p.setExp(0.0f);
        e.setDroppedExp(0);
    }
}

