/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 */
package kiki.cro.beastxp;

import org.bukkit.entity.Player;

public class XpLevel {
    private static double xplevel;
    private static int xpe;
    private static int result;

    public static void setXpLevel(int level, float cExp) {
        if (level > 30) {
            xplevel = 4.5 * (double)level * (double)level - 162.5 * (double)level + 2220.0;
            xpe = 9 * level - 158;
            result = (int)(xplevel += (double)Math.round(cExp * (float)xpe));
            return;
        }
        if (level > 15) {
            xplevel = 2.5 * (double)level * (double)level - 40.5 * (double)level + 360.0;
            xpe = 5 * level - 38;
            result = (int)(xplevel += (double)Math.round(cExp * (float)xpe));
            return;
        }
        if (level <= 15) {
            xplevel = level * level + 6 * level;
            xpe = 2 * level + 7;
            result = (int)(xplevel += (double)Math.round(cExp * (float)xpe));
            return;
        }
    }

    public static int getXp(Player p) {
        XpLevel.setXpLevel(p.getLevel(), p.getExp());
        return result;
    }
}

