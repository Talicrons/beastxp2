
import  org.bukkit.Server
import  org.bukkit.command.CommandExecutor
import  org.bukkit.command.PluginCommand
import  org.bukkit.configuration.file.FileConfiguration
import  org.bukkit.event.Listener
import  org.bukkit.plugin.Plugin
import  org.bukkit.plugin.PluginManager
import  org.bukkit.plugin.java.JavaPlugin

package kiki.cro.beastxp;

import java.util.HashMap;
import kiki.cro.beastxp.XpItem;
import kiki.cro.beastxp.XpLevel;
import kiki.cro.beastxp.YmlMaker;
import kiki.cro.beastxp.commands.XpBottle;
import kiki.cro.beastxp.commands.XpbReload;
import kiki.cro.beastxp.listener.XpBottleThrow;
import org.bukkit.Server;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
extends JavaPlugin
implements Listener {
    public HashMap<String, Integer> xpitemlore;
    public XpLevel xpl = new XpLevel();
    public XpItem xpBottle;
    private YmlMaker cfg;

    public Main() {
        this.xpBottle = new XpItem(this);
    }

    public void onEnable() {
        this.cfg = new YmlMaker(this, "config.yml");
        this.cfg.saveDefaultConfig();
        this.onRegisterCommands();
        this.onRegisterEvents();
        this.xpitemlore = new HashMap();
        this.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)this);
        double dropPercentage = this.cfg.getConfig().getDouble("DropPercentage");
        if (dropPercentage <= 0.0 || dropPercentage > 100.0) {
            this.cfg.getConfig().set("DropPercentage", (Object)100);
            this.cfg.saveConfig();
            this.cfg.reloadConfig();
        }
    }

    public void onRegisterCommands() {
        this.getCommand("XpBottle").setExecutor((CommandExecutor)new XpBottle(this));
        this.getCommand("XpbReload").setExecutor((CommandExecutor)new XpbReload(this));
    }

    public void onRegisterEvents() {
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents((Listener)new XpBottleThrow(this), (Plugin)this);
    }

    public void onDisable() {
    }

    public YmlMaker getCfg() {
        return this.cfg;
    }
}

