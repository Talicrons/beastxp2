/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.configuration.file.FileConfiguration
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package kiki.cro.beastxp;

import java.util.ArrayList;
import java.util.List;
import kiki.cro.beastxp.Main;
import kiki.cro.beastxp.YmlMaker;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class XpItem {
    private Main pl;
    private ItemStack Xpb;

    public XpItem(Main plugin) {
        this.pl = plugin;
    }

	public void setXpb(int takenXp, Player p) {
		setXpb(takenXp, p, 1);
	}
	
    public void setXpb(int takenXp, Player p, int amount) {
        this.Xpb = new ItemStack(Material.EXP_BOTTLE, amount);
        ItemMeta xpbmeta = this.Xpb.getItemMeta();
        String BottleName = this.pl.getCfg().getConfig().getString("BottleName");
        BottleName = BottleName.replaceAll("&", "\u00a7");
        xpbmeta.setDisplayName(BottleName);
        ArrayList<String> lore = new ArrayList<String>();
        String value = this.pl.getCfg().getConfig().getString("Value");
        value = value.replaceAll("&", "\u00a7");
        value = value.replace("%value%", "" + takenXp);
        lore.add(value);
        String enchanter = this.pl.getCfg().getConfig().getString("BottleEnchanter");
        enchanter = enchanter.replaceAll("&", "\u00a7");
        if (enchanter.contains("%player%")) {
            enchanter = enchanter.replace("%player%", p.getName());
        }
        lore.add(enchanter);
        xpbmeta.setLore(lore);
        this.Xpb.setItemMeta(xpbmeta);
    }

    public void setXpbServer(int takenXp) {
        this.Xpb = new ItemStack(Material.EXP_BOTTLE, 1);
        ItemMeta xpbmeta = this.Xpb.getItemMeta();
        String BottleName = this.pl.getCfg().getConfig().getString("BottleName");
        BottleName = BottleName.replaceAll("&", "\u00a7");
        xpbmeta.setDisplayName(BottleName);
        ArrayList<String> lore = new ArrayList<String>();
        String value = this.pl.getCfg().getConfig().getString("Value");
        value = value.replaceAll("&", "\u00a7");
        value = value.replace("%value%", "" + takenXp);
        lore.add(value);
        String enchanter = this.pl.getCfg().getConfig().getString("AdminName");
        enchanter = enchanter.replaceAll("&", "\u00a7");
        lore.add(enchanter);
        xpbmeta.setLore(lore);
        this.Xpb.setItemMeta(xpbmeta);
    }

    public ItemStack getXpb() {
        return this.Xpb;
    }
}

