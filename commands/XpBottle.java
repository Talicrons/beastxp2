package kiki.cro.beastxp.commands;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kiki.cro.beastxp.Main;
import kiki.cro.beastxp.NumberCheck;
import kiki.cro.beastxp.XpItem;
import kiki.cro.beastxp.XpLevel;
import kiki.cro.beastxp.YmlMaker;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class XpBottle
implements CommandExecutor {
    private Main pl;
    String message;
    List<String> messagel;

    public XpBottle(Main plugin) {
        this.pl = plugin;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player) && args.length != 2) {
            sender.sendMessage("\u00a7cTo give xp Bottle to player use /xpb <amount> <playername>");
            return true;
        }
        Player p = null;
        if (sender instanceof Player) {
            p = (Player)sender;
        }
        if (sender.hasPermission("exp.bottle")) {
            if (args.length == 0 && p.hasPermission("exp.bottle")) {
                this.messagel = this.pl.getCfg().getConfig().getStringList("Messages.Xpb");
                Iterator<String> iterator = this.messagel.iterator();
                while (iterator.hasNext()) {
                    String s = iterator.next();
                    s = ChatColor.translateAlternateColorCodes((char)'&', (String)s);
                    p.sendMessage(s);
                }
                return true;
            }
            String xpargs0 = args[0].replaceAll("\\-", "");
            if (NumberCheck.isInt(xpargs0, p)) {
                int takenXp = Integer.parseInt(xpargs0);
                if (args.length == 1 && p.hasPermission("exp.bottle")) {
                    if (p.getInventory().firstEmpty() == -1) {
                        this.message = this.pl.getCfg().getConfig().getString("Messages.FullInventory");
                        this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
                        p.sendMessage(this.message);
                        return true;
                    }
                    int MinXp = this.pl.getCfg().getConfig().getInt("MinXp");
                    if (takenXp < MinXp) {
                        this.message = this.pl.getCfg().getConfig().getString("Messages.MinXpMessage");
                        this.message = this.message.replaceAll("%minxp%", "" + MinXp);
                        this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
                        p.sendMessage(this.message);
                        return true;
                    }
                    int MaxXp = this.pl.getCfg().getConfig().getInt("MaxXp");
                    if (takenXp > MaxXp) {
                        this.message = this.pl.getCfg().getConfig().getString("Messages.MaxXpMessage");
                        this.message = this.message.replaceAll("%maxxp%", "" + MaxXp);
                        this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
                        p.sendMessage(this.message);
                        return true;
                    }
                    int xp = XpLevel.getXp(p);
                    if (xp > takenXp) {
                        this.pl.xpBottle.setXpb(takenXp, p);
                        p.setTotalExperience(0);
                        p.setLevel(0);
                        p.setExp(0.0f);
                        p.giveExp(xp -= takenXp);
                        ItemStack Xpb = this.pl.xpBottle.getXpb();
                        p.getInventory().addItem(new ItemStack[]{Xpb});
                        this.messagel = this.pl.getCfg().getConfig().getStringList("Messages.XpTransferred");
                        Iterator<String> iterator = this.messagel.iterator();
                        while (iterator.hasNext()) {
                            String s = iterator.next();
                            s = ChatColor.translateAlternateColorCodes((char)'&', (String)s);
                            s = s.replaceAll("%takenxp%", "" + takenXp);
                            p.sendMessage(s);
                        }
                        return true;
                    }
                    this.message = this.pl.getCfg().getConfig().getString("Messages.NotEnough");
                    this.message = this.message.replaceAll("%pxp%", "" + xp);
                    this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
                    p.sendMessage(this.message);
                    return true;
                }
                if (sender.hasPermission("exp.admin")) {
                    if (args.length != 2) return false;
					String xpargs1 = args[1].replaceAll("\\-", "");
					if (NumberCheck.isInt(xpargs1, p)) {
						int amount = Integer.parseInt(xpargs1);
						int totalExpTaken = amount*takenXp;
						
						if (p.getInventory().firstEmpty() == -1) {
							this.message = this.pl.getCfg().getConfig().getString("Messages.FullInventory");
							this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
							p.sendMessage(this.message);
							return true;
						}
						int MinXp = this.pl.getCfg().getConfig().getInt("MinXp");
						if (takenXp < MinXp) {
							this.message = this.pl.getCfg().getConfig().getString("Messages.MinXpMessage");
							this.message = this.message.replaceAll("%minxp%", "" + MinXp);
							this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
							p.sendMessage(this.message);
							return true;
						}
						int MaxXp = this.pl.getCfg().getConfig().getInt("MaxXp");
						if (takenXp > MaxXp) {
							this.message = this.pl.getCfg().getConfig().getString("Messages.MaxXpMessage");
							this.message = this.message.replaceAll("%maxxp%", "" + MaxXp);
							this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
							p.sendMessage(this.message);
							return true;
						}
						int xp = XpLevel.getXp(p);
						if (xp > totalExpTaken) {
							this.pl.xpBottle.setXpb(takenXp, p, amount);
							p.setTotalExperience(0);
							p.setLevel(0);
							p.setExp(0.0f);
							p.giveExp(xp -= totalExpTaken);
							ItemStack Xpb = this.pl.xpBottle.getXpb();
							p.getInventory().addItem(new ItemStack[]{Xpb});
							this.messagel = this.pl.getCfg().getConfig().getStringList("Messages.XpTransferred");
							Iterator<String> iterator = this.messagel.iterator();
							while (iterator.hasNext()) {
								String s = iterator.next();
								s = ChatColor.translateAlternateColorCodes((char)'&', (String)s);
								s = s.replaceAll("%takenxp%", "" + totalExpTaken);
								p.sendMessage(s);
							}
							return true;
						}
						this.message = this.pl.getCfg().getConfig().getString("Messages.NotEnough");
						this.message = this.message.replaceAll("%pxp%", "" + xp);
						this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
						p.sendMessage(this.message);
						return true;						
						
					}
					this.message = this.pl.getCfg().getConfig().getString("Messages.NoNumber");
					this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
					p.sendMessage(this.message);
					return true;
                }
                this.message = this.pl.getCfg().getConfig().getString("Messages.NoPermission");
                this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
                p.sendMessage(this.message);
                return true;
            }
            this.message = this.pl.getCfg().getConfig().getString("Messages.NoNumber");
            this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
            p.sendMessage(this.message);
            return true;
        }
        this.message = this.pl.getCfg().getConfig().getString("Messages.NoPermission");
        this.message = ChatColor.translateAlternateColorCodes((char)'&', (String)this.message);
        p.sendMessage(this.message);
        return true;
    }
}

