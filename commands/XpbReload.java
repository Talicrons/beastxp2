package kiki.cro.beastxp.commands;

import kiki.cro.beastxp.Main;
import kiki.cro.beastxp.YmlMaker;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class XpbReload
implements CommandExecutor {
    private Main pl;

    public XpbReload(Main plugin) {
        this.pl = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if (sender.hasPermission("exp.admin")) {
            this.pl.getCfg().reloadConfig();
            p.sendMessage("\u00a7aReloaded Beast-Xp config!");
            return true;
        }
        p.sendMessage("\u00a7cYou need exp.admin permission!");
        return false;
    }
}

